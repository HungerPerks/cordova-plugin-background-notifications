package com.HungerPerks;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by samborick on 1/15/18.
 */

public class MyReceiver extends BroadcastReceiver {
    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent msgIntent = new Intent(context, AlertService.class);
        context.startService(msgIntent);
    }

}
